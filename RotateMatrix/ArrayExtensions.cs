using System;

#pragma warning disable CA1814
#pragma warning disable S2368

namespace RotateMatrix
{
    public static class ArrayExtensions
    {
        public static void Rotate90DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = 0; layer < length / 2; layer++)
            {
                int first = layer;
                int last = length - 1 - layer;

                for (int i = first; i < last; i++)
                {
                    int offset = i - first;

                    int container = matrix[first, i];

                    matrix[first, i] = matrix[last - offset, first];

                    matrix[last - offset, first] = matrix[last, last - offset];

                    matrix[last, last - offset] = matrix[i, last];

                    matrix[i, last] = container;
                }
            }
        }

        public static void Rotate90DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = 0; layer < length / 2; layer++)
            {
                int first = layer;
                int last = length - 1 - layer;

                for (int i = first; i < last; i++)
                {
                    int offset = i - first;

                    int storage = matrix[first, i];

                    matrix[first, i] = matrix[first + offset, last];

                    matrix[first + offset, last] = matrix[last, last - offset];

                    matrix[last, last - offset] = matrix[last - offset, i - offset];

                    matrix[last - offset, i - offset] = storage;
                }
            }
        }

        public static void Rotate180DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = length; layer > length / 2; layer--)
            {
                int first = length - 1;
                int last = length - layer;

                if (layer == length)
                {
                    for (int i = first; i >= 0; i--)
                    {
                        int offset = first - i;

                        int container = matrix[first, i - last];

                        matrix[first, i - last] = matrix[last, offset];

                        matrix[last, offset] = container;
                    }
                }
                else if (last == length / 2 && length % 2 != 0)
                {
                    for (int i = first; i > last; i--)
                    {
                        int offset = first - i;

                        int container = matrix[i - last + offset, first - offset];

                        matrix[i - last + offset, first - offset] = matrix[last, offset];

                        matrix[last, offset] = container;
                    }

                    break;
                }
                else
                {
                    for (int i = first; i >= 0; i--)
                    {
                        int offset = first - i;

                        int container = matrix[i - last + offset, first - offset];

                        matrix[i - last + offset, first - offset] = matrix[last, offset];

                        matrix[last, offset] = container;
                    }
                }
            }
        }

        public static void Rotate180DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = length; layer > length / 2; layer--)
            {
                int first = length - 1;
                int last = length - layer;

                if (layer == length)
                {
                    for (int i = first; i >= 0; i--)
                    {
                        int offset = first - i;

                        int storage = matrix[first, i - last];

                        matrix[first, i - last] = matrix[last, offset];

                        matrix[last, offset] = storage;
                    }
                }
                else if (last == length / 2 && length % 2 != 0)
                {
                    for (int i = first; i > last; i--)
                    {
                        int offset = first - i;

                        int storage = matrix[i - last + offset, first - offset];

                        matrix[i - last + offset, first - offset] = matrix[last, offset];

                        matrix[last, offset] = storage;
                    }

                    break;
                }
                else
                {
                    for (int i = first; i >= 0; i--)
                    {
                        int offset = first - i;

                        int storage = matrix[i - last + offset, first - offset];

                        matrix[i - last + offset, first - offset] = matrix[last, offset];

                        matrix[last, offset] = storage;
                    }
                }
            }
        }

        public static void Rotate270DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = 0; layer < length / 2; layer++)
            {
                int first = layer;
                int last = length - 1 - layer;

                for (int i = first; i < last; i++)
                {
                    int offset = i - first;

                    int container = matrix[first, i];

                    matrix[first, i] = matrix[first + offset, last];

                    matrix[first + offset, last] = matrix[last, last - offset];

                    matrix[last, last - offset] = matrix[last - offset, i - offset];

                    matrix[last - offset, i - offset] = container;
                }
            }
        }

        public static void Rotate270DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int length = matrix.GetLength(0);

            for (int layer = 0; layer < length / 2; layer++)
            {
                int first = layer;
                int last = length - 1 - layer;

                for (int i = first; i < last; i++)
                {
                    int offset = i - first;

                    int storage = matrix[first, i];

                    matrix[first, i] = matrix[last - offset, first];

                    matrix[last - offset, first] = matrix[last, last - offset];

                    matrix[last, last - offset] = matrix[i, last];

                    matrix[i, last] = storage;
                }
            }
        }

        public static void Rotate360DegreesClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
        }

        public static void Rotate360DegreesCounterClockwise(this int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
        }
    }
}
